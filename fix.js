(function() {
    // If we're not on a form page, we won't have the cancel button.
    if (!window.document.getElementById('form1')) {
        return;
    }
    var cancel = window.document.getElementById('btnCancel');
    if (!cancel) {
        return;
    }
    var parent = cancel.parentNode;
    parent.removeChild(cancel);
})();
